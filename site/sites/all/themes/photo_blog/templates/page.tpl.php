<div class="site-container">
        
        <div class="gradient-overlay">
        </div>
        
        <!-- header -->
        <div class="header-wrapper container">
           <div class="site-title">
             <?php
			   if($page['site_header']):
			     print render($page['site_header']);
			   endif;
			   ?>
           </div>
           <div class="menu-wrapper clearfix ">
           <ul class= "menu">
           <?php
		      print theme('links',array('links'=>$main_menu));
			   ?>
            </ul>
            
            </div>
        </div>
        
        
        <!-- site title -->
        <div class="site-title-container">
            <div class="site-title">
                <p>Photography Blog</p>
            </div>
        </div>
        
        <!-- content -->
        <div class="content-container container">
            <div class="menu-container inner-container">
                <ul class= "menu">
				<?php 
                print theme('links',array('links'=>$main_menu));
            	?>
                </ul>
         </div>
           </div>
            
            <!-- tabs will go here -->
            <div class="tab-container container">
             	<?php if ($tabs): ?>
                	<div class="tabs">
                 		<?php print render($tabs); ?>
                	</div>
            	<?php endif; ?>
            </div> <!-- /.section, /#messages -->
           
            
           
            
            <div class="title inner-container">
                <h1><?php print $title; ?></h1>
            </div>
            <div class="left-column column region one-fourth left">
            <div class="region-inner">
                        <?php if($page['left_callout']): ?>
                			
                    	<?php print render($page['left_callout']); ?>
                			
            			<?php endif; ?>
            
                   
             </div>
              </div>
              
            <div class="content inner-container clearfix">
                <div class="main-content three-fourths left">
                    <div class="content">
                        
                        <!-- messages will go here -->
                        <div id="messages">
                        
                        </div>
                        
                        <?php
                     		print render($page['content']);
                        ?>
                	 </div>
                
                	 
                </div>
               
            </div>
        </div>
        
        <!-- footer -->
        <div class="footer-container container">
            <div class="footer-content inner-container">
            <?php if($page['footer']):
              print render($page['footer']); 
            endif; ?>  
               
            </div>
        </div>
        
    </div>