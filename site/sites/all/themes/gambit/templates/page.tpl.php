<div id="site-header">
        <div id="site-header-image">
            <img src="./<?php print path_to_theme(); ?>/imgs/banner.jpg" title="Home" alt="Ivory Trunks" />
        </div>
        <div class="container top">
            <div id="logo">
              <a href="/">
                <img src="./<?php print path_to_theme(); ?>/imgs/logo.png" />
              </a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="menu-nav-container main">
            <?php 
                print theme('links',array('links'=>$main_menu));
            ?>
        </div>
    </div>
    <div class="container content">
        <div class="clearfix">
            
            <h1><?php print $title; ?></h1>

            <?php if ($messages): ?>
                <div id="messages"><div class="section clearfix">
                  <?php print $messages; ?>
                </div></div> <!-- /.section, /#messages -->
            <?php endif; ?>
            
            <?php if ($tabs): ?>
                <div class="tabs">
                    <?php print render($tabs); ?>
                </div>
            <?php endif; ?>
            
            <?php if($page['above_content']):
              print render($page['above_content']); 
            endif; ?>  

            <?php if($page['left_callout']): ?>
                <div class="<?php print $variables['gambit_widthClasses']['column'] ?>">
                    <?php print render($page['left_callout']); ?>
                </div>
            <?php endif; ?>
            
            <div class="<?php print $variables['gambit_widthClasses']['main'] ?>">
                <?php
                    print render($page['content']);
                ?>
            </div>
            
            <?php if($page['right_callout']): ?>
                <div class="<?php print $variables['gambit_widthClasses']['column'] ?>">
                    <?php print render($page['right_callout']); ?>
                </div>
            <?php endif; ?>
            
        </div>
    </div>
    <div id="site-footer">
        <div class="container green">
            <div class="menu-nav-container footer">
                <?php 
                    print theme('links',array('links'=>$main_menu));
                ?> 
                       
            </div>
            <?php if($page['paragraph_footer']):
              print render($page['paragraph_footer']); 
            endif; ?>  
        </div>
            
    </div>
