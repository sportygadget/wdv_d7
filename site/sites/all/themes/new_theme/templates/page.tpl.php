<div class="site-container">
        
        <div class="gradient-overlay">
        </div>
        
        <!-- header -->
        <div class="header-wrapper">
        </div>
        
        <!-- site title -->
        <div class="site-title-container">
            <div class="site-title">
                <p>Photography Blog</p>
            </div>
        </div>
        
        <!-- content -->
        <div class="content-container container">
            <div class="menu-container inner-container">
                <?php 
                print theme('links',array('links'=>$main_menu));
            	?>
            </div>
            
            <!-- tabs will go here -->
            <div class="tab-container container">
             	<?php if ($tabs): ?>
                	<div class="tabs">
                 		<?php print render($tabs); ?>
                	</div>
            	<?php endif; ?>
            </div> <!-- /.section, /#messages -->
           
            
           
            
            <div class="title inner-container">
                <h1><?php print $title; ?></h1>
            </div>
            
            <div class="content inner-container clearfix">
                <div class="main-content three-fourths left">
                    <div class="content">
                        
                        <!-- messages will go here -->
                        <div id="messages">
                        
                        </div>
                        
                        <?php
                     		print render($page['content']);
                        ?>
                	 </div>
                
                	 
                </div>
                <div class="right-column column region one-fourth left">
                        <?php if($page['right_callout']): ?>
                			
                    	<?php print render($page['right_callout']); ?>
                			
            			<?php endif; ?>
            
                   
                    </div>
            </div>
        </div>
        
        <!-- footer -->
        <div class="footer-container container">
            <div class="footer-content inner-container">
            <?php if($page['footer']):
              print render($page['footer']); 
            endif; ?>  
               
            </div>
        </div>
        
    </div>